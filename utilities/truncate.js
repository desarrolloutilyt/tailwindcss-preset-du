const plugin = require('tailwindcss/plugin');

module.exports = plugin(function ({ addUtilities }) {
	const rules = [];

	const buclesico = i => {
		const name = `.truncate-${String(i)}-lines`;
		const style = {
			overflow: 'hidden',
			display: '-webkit-box',
			'-webkit-line-clamp': `${String(i)}`,
			'-webkit-box-orient': 'vertical',
		};

		const rule = {};
		rule[name] = style;
		rules.push(rule);
	};

	for (let i = 1; i <= 20; i += 1) buclesico(i);

	addUtilities(rules, {
		variants: ['responsive', 'hover'],
	});
});
