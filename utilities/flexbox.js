const plugin = require('tailwindcss/plugin');

module.exports = plugin(function ({ addUtilities }) {
	const rules = [];

	const source = [
		[{ display: 'flex' }, '.flex'],
		[{ display: 'flex', flexDirection: 'column' }, '.flexcol'],
	];

	const justifyContent = [
		['center', 'c'],
		['flex-start', 's'],
		['flex-end', 'e'],
		['space-around', 'sa'],
		['space-between', 'sb'],
		['space-evenly', 'se'],
	];

	const alignItems = [
		['center', 'c'],
		['flex-start', 's'],
		['flex-end', 'e'],
		['stretch', 'st'],
		['baseline', 'bs'],
	];

	for (const flex of source) {
		for (const justify of justifyContent) {
			for (const align of alignItems) {
				const className = [flex[1], justify[1], align[1]].join('-');
				const style = {
					...flex[0],
					justifyContent: justify[0],
					alignItems: align[0],
				};
				const rule = {};
				rule[className] = style;

				rules.push(rule);
			}
		}
	}

	addUtilities(rules, {
		variants: ['responsive', 'hover'],
	});
});
