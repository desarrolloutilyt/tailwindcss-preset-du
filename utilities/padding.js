const plugin = require('tailwindcss/plugin');

module.exports = plugin(function ({ addUtilities }) {
	const rules = [];

	const options = [
		[
			'x',
			p => ({
				paddingLeft: `${p}rem`,
				paddingRight: `${p}rem`,
			}),
		],
		[
			'y',
			p => ({
				paddingTop: `${p}rem`,
				paddingBottom: `${p}rem`,
			}),
		],
		[
			't',
			p => ({
				paddingTop: `${p}rem`,
			}),
		],
		[
			'b',
			p => ({
				paddingBottom: `${p}rem`,
			}),
		],
		[
			'l',
			p => ({
				paddingLeft: `${p}rem`,
			}),
		],
		[
			'r',
			p => ({
				paddingRight: `${p}rem`,
			}),
		],
	];

	const addRules = i => {
		const name = `.p-${String(i).replace('.', '_')}`;
		const style = { padding: `${i}rem` };
		const rule = {};
		rule[name] = style;
		rules.push(rule);

		for (const option of options) {
			const name = `.p${option[0]}-${String(i).replace('.', '_')}`;
			const style = option[1](i);
			const rule = {};
			rule[name] = style;
			rules.push(rule);
		}
	};

	for (let i = 0; i <= 1; i += 0.125) addRules(i);
	for (let i = 1.25; i <= 5; i += 0.25) addRules(i);
	for (let i = 5.5; i <= 10; i += 0.5) addRules(i);
	for (let i = 11; i <= 100; i++) addRules(i);

	addUtilities(rules, ['responsive']);
});
