const plugin = require('tailwindcss/plugin');

module.exports = plugin(function ({ addUtilities }) {
	const rules = [
		{
			'.scroll': {
				overflow: 'scroll',
			},
		},
		{
			'.scroll-x': {
				overflowX: 'scroll',
			},
		},
		{
			'.scroll-y': {
				overflowY: 'scroll',
			},
		},
	];

	addUtilities(rules, {
		variants: ['responsive', 'hover'],
	});
});
