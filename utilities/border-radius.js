const plugin = require('tailwindcss/plugin');

module.exports = plugin(function ({ addUtilities }) {
	const rules = [];

	const addRules = i => {
		const name = `.rounded-${String(i).replace('.', '_')}`;
		const style = { 'border-radius': `${i}rem` };

		const rule = {};
		rule[name] = style;
		rules.push(rule);
	};

	for (let i = 0; i < 1.5; i += 0.125) addRules(i);
	for (let i = 1.5; i <= 5; i += 0.25) addRules(i);

	addUtilities(rules, {
		variants: ['responsive', 'hover'],
	});
});
