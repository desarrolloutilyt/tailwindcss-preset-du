const plugin = require('tailwindcss/plugin');

module.exports = plugin(function ({ addUtilities }) {
	const rules = [
		{
			'.top-100': {
				top: '100%',
			},
		},
		{
			'.bottom-100': {
				bottom: '100%',
			},
		},
		{
			'.left-100': {
				left: '100%',
			},
		},
		{
			'.right-100': {
				right: '100%',
			},
		},
		{
			'.topleft': {
				top: 0,
				left: 0,
			},
		},
		{
			'.topright': {
				top: 0,
				right: 0,
			},
		},
		{
			'.bottomleft': {
				bottom: 0,
				left: 0,
			},
		},
		{
			'.bottomright': {
				bottom: 0,
				right: 0,
			},
		},
	];

	for (let i = -10; i <= 10; i += 0.125) {
		for (const position of ['top', 'bottom', 'left', 'right']) {
			const name = `.${position}-${String(i).replace('.', '_')}`;
			let style = {};
			style[position] = `${i}rem`;

			const rule = {};
			rule[name] = style;
			rules.push(rule);
		}
	}

	addUtilities(rules, {
		variants: ['responsive', 'hover'],
	});
});
