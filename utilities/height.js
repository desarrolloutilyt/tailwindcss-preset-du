const plugin = require('tailwindcss/plugin');

module.exports = plugin(function ({ addUtilities }) {
	const rules = [];

	const addRules = i => {
		const name = `.h-${String(i).replace('.', '_')}`;
		const style = { height: `${i}rem` };

		const rule = {};
		rule[name] = style;
		rules.push(rule);
	};

	for (let i = 0; i <= 1; i += 0.125) addRules(i);
	for (let i = 1.25; i <= 5; i += 0.25) addRules(i);
	for (let i = 5.5; i <= 100; i += 0.5) addRules(i);

	addUtilities(rules, {
		variants: ['responsive', 'hover'],
	});
});
