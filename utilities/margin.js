const plugin = require('tailwindcss/plugin');

module.exports = plugin(function ({ addUtilities }) {
	const rules = [];

	const options = [
		[
			'x',
			p => ({
				marginLeft: `${p}rem`,
				marginRight: `${p}rem`,
			}),
		],
		[
			'y',
			p => ({
				marginTop: `${p}rem`,
				marginBottom: `${p}rem`,
			}),
		],
		[
			't',
			p => ({
				marginTop: `${p}rem`,
			}),
		],
		[
			'b',
			p => ({
				marginBottom: `${p}rem`,
			}),
		],
		[
			'l',
			p => ({
				marginLeft: `${p}rem`,
			}),
		],
		[
			'r',
			p => ({
				marginRight: `${p}rem`,
			}),
		],
	];

	const addRules = i => {
		const name = `.m-${String(i).replace('.', '_')}`;
		const style = { margin: `${i}rem` };
		const rule = {};
		rule[name] = style;
		rules.push(rule);

		for (const option of options) {
			const name = `.m${option[0]}-${String(i).replace('.', '_')}`;
			const style = option[1](i);
			const rule = {};
			rule[name] = style;
			rules.push(rule);
		}
	};

	for (let i = -11; i >= -100; i--) addRules(i);
	for (let i = -5.5; i >= -10; i -= 0.5) addRules(i);
	for (let i = -1.25; i >= -5; i -= 0.25) addRules(i);
	for (let i = -1; i <= 1; i += 0.125) addRules(i);
	for (let i = 1.25; i <= 5; i += 0.25) addRules(i);
	for (let i = 5.5; i <= 10; i += 0.5) addRules(i);
	for (let i = 11; i <= 100; i++) addRules(i);

	addUtilities(rules, ['responsive']);
});
