const plugin = require('tailwindcss/plugin');

module.exports = plugin(function ({ addUtilities }) {
	const rules = [];

	const options = [
		['100%', 'full'],
		['100vh', 'screen'],
	];

	for (const option of options) {
		const name = `.max-h-${option[1]}`;
		const style = {
			maxHeight: option[0],
		};
		const rule = {};
		rule[name] = style;
		rules.push(rule);
	}

	const addRules = i => {
		const name = `.max-h-${String(i).replace('.', '_')}`;
		const style = { maxHeight: `${i}rem` };

		const rule = {};
		rule[name] = style;
		rules.push(rule);
	};

	for (let i = 0; i <= 1; i += 0.125) addRules(i);
	for (let i = 1.25; i <= 5; i += 0.25) addRules(i);
	for (let i = 5.5; i <= 10; i += 0.5) addRules(i);
	for (let i = 11; i <= 100; i++) addRules(i);

	addUtilities(rules, {
		variants: ['responsive', 'hover'],
	});
});
