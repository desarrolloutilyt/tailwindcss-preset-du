const plugin = require('tailwindcss/plugin');

module.exports = plugin(function ({ addUtilities }) {
	const rules = [];

	const resolutions = [
		['xs', '480px'],
		['sm', '640px'],
		['md', '768px'],
		['lg', '1024px'],
		['xl', '1280px'],
	];

	for (const resolution of resolutions) {
		const name = `.container-${resolution[0]}`;
		const styles = {
			width: '100%',
			marginLeft: 'auto',
			marginRight: 'auto',
			maxWidth: resolution[1],
		};

		const rule = {};
		rule[name] = styles;
		rules.push(rule);
	}

	addUtilities(rules, {
		variants: ['responsive', 'hover'],
	});
});
