const plugin = require('tailwindcss/plugin');

module.exports = plugin(function ({ addUtilities }) {
	const rules = [];

	const types = [
		['all', 'all'],
		['height', 'h'],
		['width', 'w'],
		['max-width', 'max-w'],
		['max-height', 'max-h'],
		['padding', 'p'],
		['margin', 'm'],
		['background-color', 'bg'],
	];
	const timingFunctions = [
		['linear', 'lin'],
		['ease', 'e'],
		['ease-in', 'ei'],
		['ease-out', 'eo'],
		['ease-in-out', 'eio'],
	];

	for (let i = 125; i < 2000; i += 125) {
		for (const type of types) {
			for (const timingFunction of timingFunctions) {
				const name = `.transition-${type[1]}-${timingFunction[1]}-${i}`;
				const style = {
					transition: `${type[0]} ${i}ms ${timingFunction[0]}`,
				};

				const rule = {};
				rule[name] = style;
				rules.push(rule);
			}
		}
	}

	addUtilities(rules, {
		variants: ['responsive', 'hover'],
	});
});
