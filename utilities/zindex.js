const plugin = require('tailwindcss/plugin');

module.exports = plugin(function ({ addUtilities }) {
	const rules = [];

	for (let i = -100; i <= 100; i += 10) {
		const name = `.z${i}`;
		const style = {
			zIndex: i,
		};
		const rule = {};
		rule[name] = style;
		rules.push(rule);
	}

	addUtilities(rules, {
		variants: ['responsive', 'hover'],
	});
});
