module.exports = [
	// flex
	require('./utilities/flexbox'),
	// absolute
	require('./utilities/absolute'),
	require('./utilities/zindex'),
	// margin-padding
	require('./utilities/padding'),
	require('./utilities/margin'),
	// height
	require('./utilities/height'),
	require('./utilities/maxHeight'),
	require('./utilities/minHeight'),
	// width
	require('./utilities/width'),
	require('./utilities/maxWidth'),
	require('./utilities/minWidth'),
	// transition
	require('./utilities/transition'),
	// overflow
	require('./utilities/overflow'),
	// container
	require('./utilities/container'),
	// border-radius
	require('./utilities/border-radius'),
	// truncate
	require('./utilities/truncate'),
];
